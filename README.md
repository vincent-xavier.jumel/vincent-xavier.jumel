# Vincent-Xavier Jumel


Je suis Vincent-Xavier Jumel, *enseignant* **agrégé** de mathématiques et d'informatique, en poste au [lycée privé (Charles de Foucauld)](https://www.lamadone.fr/) dans le XVIIIème arrondissement de la capitale.

Formation dans le cadre de la JND, version 1 et 2

Une partie de mes réalisations :
- https://lamadone.forge.apps.education.fr/
- En mathématiques
  - https://forge.apps.education.fr/lamadone/mathematiques/terminale-s -> https://lamadone.forge.apps.education.fr/mathematiques/terminale-s/
  - https://forge.apps.education.fr/lamadone/mathematiques/seconde -> https://lamadone.forge.apps.education.fr/mathematiques/seconde/
  - https://forge.apps.education.fr/lamadone/mathematiques/premiere
- En informatique
  - https://forge.apps.education.fr/lamadone/informatique/premiere-nsi -> https://lamadone.forge.apps.education.fr/informatique/premiere-nsi/
  - https://forge.apps.education.fr/lamadone/informatique/terminale-nsi -> https://lamadone.forge.apps.education.fr/informatique/terminale-nsi/
  - https://forge.apps.education.fr/lamadone/informatique/devoirs-premiere
  - https://forge.apps.education.fr/vincent-xavier-jumel/informatique/itc-pc -> https://vincent-xavier-jumel.forge.apps.education.fr/informatique/itc-pc/
  


Je suis administrateur de l'[AEIF](https://aeif.fr) et également membre des
- Comité de suivi de la FCNE
- Comité technique de la FCNE
- Documentation de la Forge [![GitLab Stars](https://img.shields.io/gitlab/stars/docs%2Fdocs.forge.apps.education.fr?gitlab_url=https%3A%2F%2Fforge.apps.education.fr)](https://forge.apps.education.fr/docs/docs.forge.apps.education.fr)
- Contributeur occasionnel à [CodEx](https://forge.apps.education.fr/codex/codex.forge.apps.education.fr/) [![GitLab Issues](https://img.shields.io/gitlab/issues/open/335?gitlab_url=https%3A%2F%2Fforge.apps.education.fr)
](https://forge.apps.education.fr/codex/codex.forge.apps.education.fr/-/issues)
- Contributeur du modèle [Site web de cours avec exercices Python dans le navigateur version pyodide-mkdocs-theme](https://forge.apps.education.fr/docs/modeles/pyodide-mkdocs-theme-review) [![GitLab Forks](https://img.shields.io/gitlab/forks/docs%2Fmodeles%2Fpyodide-mkdocs-theme-review?gitlab_url=https%3A%2F%2Fforge.apps.education.fr%2F)
](https://forge.apps.education.fr/docs/modeles/pyodide-mkdocs-theme-review)


Vous pouvez m'écrire pour les questions sur la forge en crééant une discussion dans un ticket :
- [voir les discussions existantes](https://forge.apps.education.fr/vincent-xavier.jumel/vincent-xavier.jumel/-/issues)
- [créer une nouvelle discussion](https://forge.apps.education.fr/vincent-xavier.jumel/vincent-xavier.jumel/-/issues/new)

Pour les questions relatives à l'enseignement, mon mail est dans mon profil ou vincent-xavier.jumel chez ac tiret paris.fr (en remplaçant chez par @ et tiret par -)

Pour mes opinions personnelles, vous pouvez me retrouver sur [Mastodon](https://joinmastodon.org/) [![Mastodon Follow](https://img.shields.io/mastodon/follow/356?domain=https%3A%2F%2Fpouet.chapril.org)](https://pouet.chapril.org/@vincentxavier)
